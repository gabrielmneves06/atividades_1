/* Faça um programa que receba o salário-base de um
funcionário, calcule e mostre o salário a receber, sabendo-se
que esse funcionário tem gratificação de 5% sobre o salário
base e paga imposto de 7% sobre o salário-base. */

namespace exercicio_6
{
    let salario_base, salario_gratificacao, gratificacao, imposto, salario_final: number;
     
    salario_base = 3000;
    gratificacao = 5;
    imposto = 7;

    salario_gratificacao = salario_base + salario_base * gratificacao / 100;
    imposto = salario_base * imposto /100;
    
    salario_final = salario_gratificacao - imposto;

    console.log(` A gratificação será de ${gratificacao}`);
    console.log(` O imposto será de ${imposto}`);
    console.log(` O salário final será de: ${salario_final}`);
}